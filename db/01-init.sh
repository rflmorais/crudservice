#!/bin/bash
set -e
export PGPASSWORD=$POSTGRES_PASSWORD;
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE DATABASE simplecrud;
  \connect simplecrud
  BEGIN;
    create table pessoa (
    	id bigserial not null primary key,
    	nome varchar(255) not null,
    	cpf varchar(11) not null,
    	datanascimento date not null
    );

    create table contato (
    	id bigserial not null primary key,
    	idpessoa bigint not null,
    	nome varchar(255) not null,
    	telefone varchar(12) not null,
    	email varchar(255) not null
    );

    alter table contato
    add constraint contato_pessoa_fk
    foreign key (idpessoa)
    references pessoa(id);

    insert into pessoa (nome, cpf, datanascimento) values ('Fulano da Silva', '03066577163', '1987-08-14');
    insert into pessoa (nome, cpf, datanascimento) values ('Jose Antonio', '54717149114', '1994-04-05');
    insert into pessoa (nome, cpf, datanascimento) values ('Mario Almeida', '40916333230', '2002-08-14');
    insert into pessoa (nome, cpf, datanascimento) values ('Maria Aparecida', '78395231296', '1978-11-25');
    insert into pessoa (nome, cpf, datanascimento) values ('Paulo Henrique', '74104315710', '2004-05-11');
    insert into pessoa (nome, cpf, datanascimento) values ('Marcelo Ramos', '12257173295', '1998-09-30');


    insert into contato (nome, telefone, email, idpessoa) values ('Fulano', '1234453765', 'fulano@silva.com.br',
        (select id from pessoa where cpf = '03066577163'));
    insert into contato (nome, telefone, email, idpessoa) values ('Jose', '1234453765', 'jose@antonio.com.br',
        (select id from pessoa where cpf = '54717149114'));
    insert into contato (nome, telefone, email, idpessoa) values ('Marcio', '1234453765', 'marcio@almeida.com.br',
        (select id from pessoa where cpf = '40916333230'));
    insert into contato (nome, telefone, email, idpessoa) values ('Maria', '1234453765', 'maria@aparecida.com.br',
        (select id from pessoa where cpf = '78395231296'));
    insert into contato (nome, telefone, email, idpessoa) values ('Paulo', '1234453765', 'paulo@henrique.com.br',
        (select id from pessoa where cpf = '74104315710'));
    insert into contato (nome, telefone, email, idpessoa) values ('Marcelo', '1234453765', 'marcelo@ramos.com.br',
        (select id from pessoa where cpf = '12257173295'));
  COMMIT;
EOSQL