# CrudService

## Pré-requisitos

Esta é uma aplicação de backend reponsável por fornecedor os dados para a aplicação web cliente [CrudService-Client](https://bitbucket.org/rflmorais/crudservice-client/src/master/).

## Desenvolvimento

Dependências e versões para desenvolvimento e execução local.

| Dependência | versão  |
| ---         |  ----   |
| JDK         | 11.0.2  |
| Maven       | 3.5 (+) |


## Executando a aplicação

- Realize o build utilizando o comando **_mvn clean install_**

### Localmente
Para executar localmente acesse a pasta target na raiz do projeto e utilize o comando **_java -jar crudservice-1.0.0.jar_**.

Neste caso utilizar a URL abaixo para acessar o entry point da aplicação:
```
http://localhost:9090/
```

### Via Contêiner
Acesse a raiz da pasta do projeto e execute os seguintes comandos:

**Docker Compose**
- Realize o build: **_docker-compose up --build_**

Neste caso utilizar a URL abaixo para acessar o client:
```
http://localhost:8081/
```

## Base de Dados

O docker-compose já contempla o setup do banco de dados Postgres utilizado pela aplicação, na sua execução já faz a criação do banco de dados, as tabelas e uma carga de dados inicial para testes.

Caso o arquivo seja ignorado pela inicialização será apresentado no console uma mensagem semelhante:
```
/usr/local/bin/docker-entrypoint.sh: ignoring /docker-entrypoint-initdb.d/*
```
Neste caso, tente remover os volumes do docker utilizando o comando **_docker-compose down --volumes_** e em seguida execute novamente o comando de build;

Se o problema persistir, torna-se necessário utilizar o comando **_docker system prune --all_** para remover qualquer registro ainda existente que possa estar impedindo a importação do arquivo de inicialização. Em seguida execute novamente o comando de build;

A execução dos Scripts também pode ser realizada de forma manual, os scripts estão disponíveis no diretório raiz do projeto, pasta **db/01-init.sh**;

## Documentação da API - Swagger

Este projeto conta com a documentação dos EndPoints disponível, podendo ser acessada em:

Se executado localmente:
```
http://localhost:9090/swagger-ui.html#/
```
Se executado via Contêiner:
```
http://localhost:8081/swagger-ui.html#/
```
