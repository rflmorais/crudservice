package br.com.elotech.crudservice.utils.handler;

import br.com.elotech.crudservice.utils.Exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Comparator;
import java.util.stream.Collectors;

@ControllerAdvice(annotations = RestController.class)
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(RestExceptionHandler.class);

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        log.error(ex.getMessage());
        BindingResult result = ex.getBindingResult();
        String fieldErrors = result.getFieldErrors().stream()
                .map(fieldError ->
                        new FieldCause()
                                .setField(fieldError.getField())
                                .setCause(fieldError.getDefaultMessage()))
                .sorted(Comparator.comparing(FieldCause::getField))
                .map(FieldCause::toString)
                .collect(Collectors.joining("\n "));

        return handleExceptionInternal(ex, "Argumentos invalidos:\n " + fieldErrors, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseBody
    public ResponseEntity<String> exceptionHandler(NotFoundException e) {
        log.error(e.getMessage());
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(e.getMessage());
    }

    class FieldCause {
        private String field;
        private String cause;

        public String getField() {
            return field;
        }

        public FieldCause setField(String field) {
            this.field = field;
            return this;
        }

        public String getCause() {
            return cause;
        }

        public FieldCause setCause(String cause) {
            this.cause = cause;
            return this;
        }

        @Override
        public String toString() {
            return field + " - " + cause;
        }
    }

}
