package br.com.elotech.crudservice.utils.enums;

import br.com.elotech.crudservice.model.domain.PessoaEntity;
import br.com.elotech.crudservice.model.domain.PessoaEntity_;
import br.com.elotech.crudservice.model.dto.FiltroConsultaPessoaDto;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public enum FiltroConsultaPessoaEnum {

    ID {
        @Override
        public Boolean aplicar(FiltroConsultaPessoaDto filtro) {
            return Objects.nonNull(filtro.getIdPessoa()) && filtro.getIdPessoa() > 0d;
        }

        @Override
        public Predicate filtrar(FiltroConsultaPessoaDto filtro, CriteriaBuilder cb, Root<PessoaEntity> root) {
            return cb.equal(root.get(PessoaEntity_.ID), filtro.getIdPessoa());
        }
    },
    NOME {
        @Override
        public Boolean aplicar(FiltroConsultaPessoaDto filtro) {
            return isNotBlank(filtro.getNome());
        }

        @Override
        public Predicate filtrar(FiltroConsultaPessoaDto filtro, CriteriaBuilder cb, Root<PessoaEntity> root) {
            return cb.like(cb.upper(root.get(PessoaEntity_.NOME)), "%" + filtro.getNome().toUpperCase() + "%");
        }
    },
    CPF {
        @Override
        public Boolean aplicar(FiltroConsultaPessoaDto filtro) {
            return isNotBlank(filtro.getCpf());
        }

        @Override
        public Predicate filtrar(FiltroConsultaPessoaDto filtro, CriteriaBuilder cb, Root<PessoaEntity> root) {
            return cb.equal(root.get(PessoaEntity_.CPF), filtro.getCpf());
        }
    },
    DATA_NASCIMENTO {
        @Override
        public Boolean aplicar(FiltroConsultaPessoaDto filtro) {
            return Objects.nonNull(filtro.getDataNascimento()) && filtro.getDataNascimento().isBefore(LocalDate.now());
        }

        @Override
        public Predicate filtrar(FiltroConsultaPessoaDto filtro, CriteriaBuilder cb, Root<PessoaEntity> root) {
            return cb.equal(root.get(PessoaEntity_.DATA_NASCIMENTO), filtro.getDataNascimento());
        }
    },
    ;

    public abstract Boolean aplicar(FiltroConsultaPessoaDto filtro);

    public abstract Predicate filtrar(FiltroConsultaPessoaDto filtro, CriteriaBuilder cb, Root<PessoaEntity> root);

}
