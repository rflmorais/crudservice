package br.com.elotech.crudservice.utils.enums;

public enum MensagemSistemaEnum {

    OBTENDO_CADASTRO_PESSOA("Obtendo cadastro pessoa id: %d"),
    OBTENDO_CADASTRO_PESSOA_FILTRO("Obtendo cadastros pessoa filtro: %s"),
    SALVANDO_CADASTRO_PESSOA("Salvando cadastro pessoa"),
    ATUALIZANDO_CADASTRO_PESSOA("Atualizando cadastro pessoa"),
    REMOVENDO_CADASTRO_PESSOA("Removendo cadastro pessoa id: %d"),
    CADASTRO_PESSOA_NAO_ENCONTRADO("Nao encontrado cadastro pessoa id: %d"),
    ;

    private String value;

    MensagemSistemaEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
