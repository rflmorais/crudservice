package br.com.elotech.crudservice.utils.Exceptions;

public class NotFoundException extends RuntimeException {

    private String error;

    public NotFoundException(String error) {
        this.error = error;
    }

    @Override
    public String getMessage() {
        return error;
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
