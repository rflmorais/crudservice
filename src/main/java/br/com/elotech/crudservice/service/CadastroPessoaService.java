package br.com.elotech.crudservice.service;

import br.com.elotech.crudservice.model.domain.ContatoEntity;
import br.com.elotech.crudservice.model.domain.PessoaEntity;
import br.com.elotech.crudservice.model.domain.PessoaEntity_;
import br.com.elotech.crudservice.model.dto.FiltroConsultaPessoaDto;
import br.com.elotech.crudservice.model.dto.PessoaDto;
import br.com.elotech.crudservice.repository.PessoaFiltroRepository;
import br.com.elotech.crudservice.repository.PessoaRepository;
import br.com.elotech.crudservice.utils.Exceptions.NotFoundException;
import br.com.elotech.crudservice.utils.enums.MensagemSistemaEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;


@Service
public class CadastroPessoaService {

    private static final Logger logger = LoggerFactory.getLogger(CadastroPessoaService.class.getName());

    @Autowired
    private PessoaRepository pessoaRepository;
    @Autowired
    private PessoaFiltroRepository pessoaFiltroRepository;
    @Autowired
    private ConversionService conversionService;

    public PessoaDto obterPessoaById(Long id) {
        logger.info(format(MensagemSistemaEnum.OBTENDO_CADASTRO_PESSOA.getValue(), id));
        return pessoaRepository.findById(id)
                .map(pessoa -> conversionService.convert(pessoa, PessoaDto.class))
                .orElseThrow(() -> new NotFoundException(format(MensagemSistemaEnum.CADASTRO_PESSOA_NAO_ENCONTRADO.getValue(), id)));
    }

    public List<PessoaDto> obterPessoasByFiltro(FiltroConsultaPessoaDto filtroConsulta, int page, int size) {
        logger.info(format(MensagemSistemaEnum.OBTENDO_CADASTRO_PESSOA_FILTRO.getValue(), filtroConsulta.toString()));

        return pessoaFiltroRepository.findByFiltro(filtroConsulta, PageRequest.of(page, size, Sort.Direction.ASC, PessoaEntity_.NOME))
                .stream()
                .map(pessoa -> conversionService.convert(pessoa, PessoaDto.class))
                .collect(Collectors.toList());
    }

    public PessoaDto cadastrarPessoa(PessoaDto dadosCadastro) {
        logger.info(MensagemSistemaEnum.SALVANDO_CADASTRO_PESSOA.getValue());
        PessoaEntity dadosCadastroEntity = conversionService.convert(dadosCadastro, PessoaEntity.class);
        dadosCadastroEntity = persistirPessoa(dadosCadastroEntity);
        return conversionService.convert(dadosCadastroEntity, PessoaDto.class);
    }

    public PessoaDto alterarPessoa(Long id, PessoaDto dadosCadastro) {
        logger.info(MensagemSistemaEnum.ATUALIZANDO_CADASTRO_PESSOA.getValue());

        PessoaEntity registroCadastroEntity = pessoaRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(format(MensagemSistemaEnum.CADASTRO_PESSOA_NAO_ENCONTRADO.getValue(), id)));

        PessoaEntity dadosCadastroEntity = conversionService.convert(dadosCadastro, PessoaEntity.class);

        if (!registroCadastroEntity.equals(dadosCadastroEntity)) {
            dadosCadastroEntity.setId(registroCadastroEntity.getId());
            persistirPessoa(dadosCadastroEntity);
        }

        return conversionService.convert(dadosCadastroEntity, PessoaDto.class);
    }

    public void removerPessoa(Long id) {
        logger.info(format(MensagemSistemaEnum.REMOVENDO_CADASTRO_PESSOA.getValue(), id));
        pessoaRepository.findById(id)
                .ifPresentOrElse(pessoa -> pessoaRepository.delete(pessoa),
                        () -> {
                            throw new NotFoundException(format(MensagemSistemaEnum.CADASTRO_PESSOA_NAO_ENCONTRADO.getValue(), id));
                        });
    }

    private PessoaEntity persistirPessoa(PessoaEntity entity) {
        for (ContatoEntity contatoEntity : entity.getContatos()) {
            contatoEntity.setPessoa(entity);
        }
        entity = pessoaRepository.saveAndFlush(entity);
        return entity;
    }
}
