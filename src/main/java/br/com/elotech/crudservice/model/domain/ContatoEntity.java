package br.com.elotech.crudservice.model.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "contato")
public class ContatoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String nome;
    @Column
    private String telefone;
    @Column
    private String email;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idpessoa", referencedColumnName = "id", nullable = false, updatable = true, insertable = true)
    private PessoaEntity pessoa;

    public Long getId() {
        return id;
    }

    public ContatoEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public ContatoEntity setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getTelefone() {
        return telefone;
    }

    public ContatoEntity setTelefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public ContatoEntity setEmail(String email) {
        this.email = email;
        return this;
    }

    public PessoaEntity getPessoa() {
        return pessoa;
    }

    public ContatoEntity setPessoa(PessoaEntity pessoa) {
        this.pessoa = pessoa;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContatoEntity)) return false;
        ContatoEntity that = (ContatoEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getNome(), that.getNome()) &&
                Objects.equals(getTelefone(), that.getTelefone()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getPessoa(), that.getPessoa());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNome(), getTelefone(), getEmail(), getPessoa());
    }
}
