package br.com.elotech.crudservice.model.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ContatoDto {
    private Long id;
    @NotNull(message = "Não pode ser nulo!")
    @NotEmpty(message = "Não pode ser vazio!")
    private String nome;
    @NotNull(message = "Não pode ser nulo!")
    @NotEmpty(message = "Não pode ser vazio!")
    private String telefone;
    @NotNull(message = "Não pode ser nulo!")
    @NotEmpty(message = "Não pode ser vazio!")
    @Email(message = "E-mail inválido!")
    private String email;

    public Long getId() {
        return id;
    }

    public ContatoDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public ContatoDto setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getTelefone() {
        return telefone;
    }

    public ContatoDto setTelefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public ContatoDto setEmail(String email) {
        this.email = email;
        return this;
    }
}
