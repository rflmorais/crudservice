package br.com.elotech.crudservice.model.dto;

import java.time.LocalDate;

public class FiltroConsultaPessoaDto {

    private Long idPessoa;
    private String nome;
    private String cpf;
    private LocalDate dataNascimento;

    public Long getIdPessoa() {
        return idPessoa;
    }

    public FiltroConsultaPessoaDto setIdPessoa(Long idPessoa) {
        this.idPessoa = idPessoa;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public FiltroConsultaPessoaDto setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getCpf() {
        return cpf;
    }

    public FiltroConsultaPessoaDto setCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public FiltroConsultaPessoaDto setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
        return this;
    }

    @Override
    public String toString() {
        return "FiltroConsultaPessoaDto {" +
                "idPessoa=" + idPessoa +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", dataNascimento=" + dataNascimento +
                '}';
    }
}
