package br.com.elotech.crudservice.model.dto;

import org.hibernate.validator.constraints.br.CPF;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Set;

public class PessoaDto {
    private Long id;
    @NotNull(message = "Não pode ser nulo!")
    @NotEmpty(message = "Não pode ser vazio!")
    private String nome;
    @NotNull(message = "Não pode ser nulo!")
    @NotEmpty(message = "Não pode ser vazio!")
    @CPF(message = "CPF inválido!")
    private String cpf;
    @NotNull(message = "Não pode ser nulo!")
    @PastOrPresent(message = "Deve ser anterior ou igual a data de hoje!")
    private LocalDate dataNascimento;
    @Size(min = 1, message = "Deve ser informado ao menos 1 contato!")
    @Valid
    private Set<ContatoDto> contatos;

    public Long getId() {
        return id;
    }

    public PessoaDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public PessoaDto setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getCpf() {
        return cpf;
    }

    public PessoaDto setCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public PessoaDto setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
        return this;
    }

    public Set<ContatoDto> getContatos() {
        return contatos;
    }

    public PessoaDto setContatos(Set<ContatoDto> contatos) {
        this.contatos = contatos;
        return this;
    }
}
