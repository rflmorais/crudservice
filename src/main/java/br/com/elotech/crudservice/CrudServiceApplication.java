package br.com.elotech.crudservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

@SpringBootApplication(scanBasePackages = {"br.com.elotech.crudservice"})
public class CrudServiceApplication {

    private static final Logger logger = LoggerFactory.getLogger(CrudServiceApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(CrudServiceApplication.class, args);
        ConfigurableEnvironment environment = run.getEnvironment();

        logger.info("\n\n\t-----------------------------------------\n\t" +
                "    QueueServer running on port: " + environment.getProperty("server.port") +
                "\n\t-----------------------------------------\n\t");
    }

}
