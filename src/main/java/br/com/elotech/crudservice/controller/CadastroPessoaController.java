package br.com.elotech.crudservice.controller;

import br.com.elotech.crudservice.model.dto.FiltroConsultaPessoaDto;
import br.com.elotech.crudservice.model.dto.PessoaDto;
import br.com.elotech.crudservice.service.CadastroPessoaService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("*")
@RestController()
@RequestMapping("api/public/v1/pessoa")
public class CadastroPessoaController {

    @Autowired
    private CadastroPessoaService service;

    @ApiOperation(value = "Obtem os dados de cadastro de pessoa a partir do Id")
    @GetMapping("/obter/{id}")
    public ResponseEntity<PessoaDto> obter(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(service.obterPessoaById(id));
    }

    @ApiOperation(value = "Obtem os dados de um ou mais cadastros de pessoa partir do campos informados no filtro")
    @PostMapping(value = "/filtrar")
    public ResponseEntity<List<PessoaDto>> filtrar(@RequestBody FiltroConsultaPessoaDto filtroConsulta,
                                                   @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                   @RequestParam(value = "size", required = false, defaultValue = "10") int size) {
        return ResponseEntity.ok(service.obterPessoasByFiltro(filtroConsulta, page, size));
    }

    @ApiOperation(value = "Grava os dados de um cadastro de pessoa")
    @PostMapping("/cadastrar")
    public ResponseEntity cadastrar(@Valid @RequestBody PessoaDto pessoa) {
        return new ResponseEntity<>(service.cadastrarPessoa(pessoa), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza os dados de um cadastro de pessoa")
    @PutMapping("/alterar/{id}")
    public ResponseEntity alterar(@PathVariable(value = "id") Long id,
                                  @Valid @RequestBody PessoaDto pessoa) {
        return ResponseEntity.ok(service.alterarPessoa(id, pessoa));
    }

    @ApiOperation(value = "Remove os dados de cadastrp de pessoa a partir do Id")
    @DeleteMapping("remover/{id}")
    public ResponseEntity remover(@PathVariable(value = "id") Long id) {
        service.removerPessoa(id);
        return ResponseEntity.ok().build();
    }

}
