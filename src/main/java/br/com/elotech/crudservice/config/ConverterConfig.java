package br.com.elotech.crudservice.config;

import br.com.elotech.crudservice.model.domain.ContatoEntity;
import br.com.elotech.crudservice.model.domain.PessoaEntity;
import br.com.elotech.crudservice.model.dto.ContatoDto;
import br.com.elotech.crudservice.model.dto.PessoaDto;
import br.com.elotech.crudservice.utils.conversion.ConverterUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

@Configuration
public class ConverterConfig {

    @Primary
    @Bean
    public ConversionService conversionService() {
        DefaultConversionService service = new DefaultConversionService();
        service.addConverter(new ConverterUtil<ContatoDto, ContatoEntity>() {
        });
        service.addConverter(new ConverterUtil<ContatoEntity, ContatoDto>() {
        });
        service.addConverter(new ConverterUtil<PessoaDto, PessoaEntity>() {
        });
        service.addConverter(new ConverterUtil<PessoaEntity, PessoaDto>() {
        });
        return service;
    }
}
