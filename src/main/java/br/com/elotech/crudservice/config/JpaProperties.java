package br.com.elotech.crudservice.config;

import java.util.Properties;

public class JpaProperties {

    public static Properties additional() {
        Properties props = new Properties();
        props.setProperty("hibernate.hbm2ddl.auto", "none");
        props.setProperty("hibernate.show_sql", "false");
        props.setProperty("hibernate.format_sql", "false");
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL92Dialect");
        props.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false");
        props.setProperty("hibernate.jdbc.batch_size", "1000");
        props.setProperty("hibernate.order_inserts", "true");
        return props;
    }
}
