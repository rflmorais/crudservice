package br.com.elotech.crudservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import static br.com.elotech.crudservice.config.JpaProperties.additional;

@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "simpleCrudEntityManagerFactory",
        transactionManagerRef = "simpleCrudTransactionManager",
        basePackages = "br.com.elotech.crudservice.repository"
)
@EnableTransactionManagement
public class DataSourceConfiguration {

    @Value("${crudservice.config.datasource.username}")
    private String username;

    @Value("${crudservice.config.datasource.password}")
    private String password;

    @Value("${crudservice.config.datasource.url}")
    private String url;

    @Value("${crudservice.config.datasource.driver}")
    private String driver;

    @Autowired
    JpaVendorAdapter jpaVendorAdapter;


    @Bean(name = "simpleCrud-db")
    public DataSource simpleCrudDataSource() {
        DriverManagerDataSource driverManager = new DriverManagerDataSource();
        driverManager.setDriverClassName(driver);
        driverManager.setUrl(url);
        driverManager.setUsername(username);
        driverManager.setPassword(password);
        return driverManager;
    }

    @Primary
    @Bean(name = "simpleCrudEntityManagerFactory")
    public EntityManagerFactory simpleCrudEntityManagerFactory(final @Qualifier("simpleCrud-db") DataSource simpleCrudDataSource) {
        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
        lef.setDataSource(simpleCrudDataSource);
        lef.setJpaVendorAdapter(jpaVendorAdapter);
        lef.setJpaProperties(additional());
        lef.setPackagesToScan("br.com.elotech.crudservice.model.domain");
        lef.setPersistenceUnitName("persistenceUnit");
        lef.afterPropertiesSet();
        return lef.getObject();
    }

    @Bean(name = "simpleCrudTransactionManager")
    public PlatformTransactionManager simpleCrudTransactionManager(@Qualifier("simpleCrudEntityManagerFactory") EntityManagerFactory simpleCrudEntityManagerFactory) {
        return new JpaTransactionManager(simpleCrudEntityManagerFactory);
    }

}
