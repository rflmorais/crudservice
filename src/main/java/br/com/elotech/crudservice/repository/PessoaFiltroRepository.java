package br.com.elotech.crudservice.repository;

import br.com.elotech.crudservice.model.domain.PessoaEntity;
import br.com.elotech.crudservice.model.dto.FiltroConsultaPessoaDto;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PessoaFiltroRepository {

    PageImpl<PessoaEntity> findByFiltro(FiltroConsultaPessoaDto filtroConsulta, Pageable pageable);

}
