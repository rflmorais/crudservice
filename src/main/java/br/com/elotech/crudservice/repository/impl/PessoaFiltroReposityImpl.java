package br.com.elotech.crudservice.repository.impl;

import br.com.elotech.crudservice.model.domain.PessoaEntity;
import br.com.elotech.crudservice.model.dto.FiltroConsultaPessoaDto;
import br.com.elotech.crudservice.repository.PessoaFiltroRepository;
import br.com.elotech.crudservice.utils.enums.FiltroConsultaPessoaEnum;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Arrays;

@Repository
public class PessoaFiltroReposityImpl implements PessoaFiltroRepository {

    @PersistenceContext(unitName = "persistenceUnit")
    private EntityManager em;

    public PageImpl<PessoaEntity> findByFiltro(FiltroConsultaPessoaDto filtroConsulta, Pageable page) {

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<PessoaEntity> query = builder.createQuery(PessoaEntity.class);
        Root<PessoaEntity> root = query.from(PessoaEntity.class);

        query.where(Arrays.stream(FiltroConsultaPessoaEnum.values())
                .filter(filtro -> filtro.aplicar(filtroConsulta))
                .map(filtro -> filtro.filtrar(filtroConsulta, builder, root))
                .toArray(Predicate[]::new));

        TypedQuery<PessoaEntity> consultaFiltro = em.createQuery(query);
        int size = consultaFiltro.getResultList().size();
        consultaFiltro.setFirstResult(page.getPageNumber() * page.getPageSize());
        consultaFiltro.setMaxResults(page.getPageSize());
        return new PageImpl<>(consultaFiltro.getResultList(), page, size);
    }
}
