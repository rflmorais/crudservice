package br.com.elotech.crudservice.constraints;

import br.com.elotech.crudservice.environment.PessoaEnvironment;
import br.com.elotech.crudservice.model.dto.ContatoDto;
import br.com.elotech.crudservice.model.dto.PessoaDto;
import org.assertj.core.util.Sets;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class ConstraintValidationsTest {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validacaoFalhaPessoaTest() {
        PessoaDto pessoa = PessoaEnvironment.obterDto(1L);
        pessoa.setCpf("12345678900");
        pessoa.setNome("");
        pessoa.setDataNascimento(null);
        pessoa.setContatos(new HashSet<>());

        Set<ConstraintViolation<PessoaDto>> validate = validator.validate(pessoa);
        Assertions.assertFalse(validate.isEmpty());

        List<ConstraintViolation<PessoaDto>> validations = validate.stream().sorted(Comparator.comparing(o -> o.getPropertyPath().toString())).collect(Collectors.toList());
        MatcherAssert.assertThat(validations.get(0).getPropertyPath().toString(), CoreMatchers.is("contatos"));
        MatcherAssert.assertThat(validations.get(0).getMessage(), CoreMatchers.is("Deve ser informado ao menos 1 contato!"));
        MatcherAssert.assertThat(validations.get(1).getPropertyPath().toString(), CoreMatchers.is("cpf"));
        MatcherAssert.assertThat(validations.get(1).getMessage(), CoreMatchers.is("CPF inválido!"));
        MatcherAssert.assertThat(validations.get(2).getPropertyPath().toString(), CoreMatchers.is("dataNascimento"));
        MatcherAssert.assertThat(validations.get(2).getMessage(), CoreMatchers.is("Não pode ser nulo!"));
        MatcherAssert.assertThat(validations.get(3).getPropertyPath().toString(), CoreMatchers.is("nome"));
        MatcherAssert.assertThat(validations.get(3).getMessage(), CoreMatchers.is("Não pode ser vazio!"));

    }

    @Test
    public void validacaoFalhaContatoTest() {
        PessoaDto pessoa = PessoaEnvironment.obterDto(1L);
        pessoa.setCpf("34187956506");
        pessoa.setNome("Pessoa");
        pessoa.setDataNascimento(LocalDate.of(1980, 1, 10));
        pessoa.setContatos(Sets.newHashSet(Arrays.asList(new ContatoDto().setNome("").setTelefone("").setEmail("email.com.br"))));

        Set<ConstraintViolation<PessoaDto>> validate = validator.validate(pessoa);
        Assertions.assertFalse(validate.isEmpty());

        List<ConstraintViolation<PessoaDto>> validations = validate.stream().sorted(Comparator.comparing(o -> o.getPropertyPath().toString())).collect(Collectors.toList());
        MatcherAssert.assertThat(validations.get(0).getPropertyPath().toString(), CoreMatchers.is("contatos[].email"));
        MatcherAssert.assertThat(validations.get(0).getMessage(), CoreMatchers.is("E-mail inválido!"));
        MatcherAssert.assertThat(validations.get(1).getPropertyPath().toString(), CoreMatchers.is("contatos[].nome"));
        MatcherAssert.assertThat(validations.get(1).getMessage(), CoreMatchers.is("Não pode ser vazio!"));
        MatcherAssert.assertThat(validations.get(2).getPropertyPath().toString(), CoreMatchers.is("contatos[].telefone"));
        MatcherAssert.assertThat(validations.get(2).getMessage(), CoreMatchers.is("Não pode ser vazio!"));
    }

    @Test
    public void validacaoSucessoTest() {
        PessoaDto pessoa = PessoaEnvironment.obterDto(1L);
        pessoa.setCpf("34187956506");
        pessoa.setNome("Pessoa");
        pessoa.setDataNascimento(LocalDate.of(1980, 1, 10));
        pessoa.setContatos(Sets.newHashSet(Arrays.asList(new ContatoDto().setNome("Pessoa").setTelefone("11234567890").setEmail("email@email.com"))));

        Set<ConstraintViolation<PessoaDto>> validate = validator.validate(pessoa);
        Assertions.assertTrue(validate.isEmpty());
    }

}
