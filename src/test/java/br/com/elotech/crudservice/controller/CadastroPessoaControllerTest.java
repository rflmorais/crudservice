package br.com.elotech.crudservice.controller;


import br.com.elotech.crudservice.environment.FiltroConsultaEnvironment;
import br.com.elotech.crudservice.environment.PessoaEnvironment;
import br.com.elotech.crudservice.model.dto.FiltroConsultaPessoaDto;
import br.com.elotech.crudservice.model.dto.PessoaDto;
import br.com.elotech.crudservice.service.CadastroPessoaService;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class CadastroPessoaControllerTest {

    @InjectMocks
    private CadastroPessoaController controller;
    @Mock
    private CadastroPessoaService service;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void obterTest() {
        Long id = 1L;
        PessoaDto pessoaDto = PessoaEnvironment.obterDto(id);

        when(service.obterPessoaById(anyLong())).thenReturn(pessoaDto);

        given()
                .standaloneSetup(controller)
                .contentType(ContentType.JSON)
                .param("id", id)
                .get("/api/public/v1/pessoa/obter")
                .then().log().all()
                .statusCode(HttpStatus.OK.value())
                .assertThat()
                .body("id", equalTo(id.intValue()));
    }

    @Test
    public void filtrarTest() {
        FiltroConsultaPessoaDto filtro = FiltroConsultaEnvironment.obter("Nome", null, null, null);
        PessoaDto pessoaDto = PessoaEnvironment.obterDto(1L);

        when(service.obterPessoasByFiltro(any(), anyInt(), anyInt())).thenReturn(Arrays.asList(pessoaDto));

        given()
                .standaloneSetup(controller)
                .contentType(ContentType.JSON)
                .body(filtro)
                .param("page", 0)
                .param("size", 10)
                .post("/api/public/v1/pessoa/filtrar")
                .then().log().all()
                .statusCode(HttpStatus.OK.value())
                .assertThat()
                .body("[0].id", equalTo(pessoaDto.getId().intValue()));
    }

    @Test
    public void cadastrarTest() {
        PessoaDto pessoaDto = PessoaEnvironment.obterDto(1L);
        pessoaDto.setCpf("34187956506");

        when(service.cadastrarPessoa(any())).thenReturn(pessoaDto);

        given()
                .standaloneSetup(controller)
                .contentType(ContentType.JSON)
                .body(pessoaDto)
                .post("/api/public/v1/pessoa/cadastrar")
                .then().log().all()
                .statusCode(HttpStatus.CREATED.value())
                .assertThat()
                .body("id", equalTo(pessoaDto.getId().intValue()));
    }

    @Test
    public void alterarTest() {
        PessoaDto pessoaDto = PessoaEnvironment.obterDto(1L);
        pessoaDto.setCpf("34187956506");

        when(service.alterarPessoa(anyLong(), any())).thenReturn(pessoaDto);

        given()
                .standaloneSetup(controller)
                .contentType(ContentType.JSON)
                .body(pessoaDto)
                .put("/api/public/v1/pessoa/alterar")
                .then().log().all()
                .statusCode(HttpStatus.OK.value())
                .assertThat()
                .body("id", equalTo(pessoaDto.getId().intValue()));
    }

    @Test
    public void removerTest() {
        doNothing().when(service).removerPessoa(any());

        given()
                .standaloneSetup(controller)
                .contentType(ContentType.JSON)
                .param("id", 1L)
                .delete("/api/public/v1/pessoa/remover")
                .then().log().all()
                .statusCode(HttpStatus.OK.value());
    }
}