package br.com.elotech.crudservice.environment;

import br.com.elotech.crudservice.model.domain.ContatoEntity;
import br.com.elotech.crudservice.model.domain.PessoaEntity;
import br.com.elotech.crudservice.model.dto.ContatoDto;
import br.com.elotech.crudservice.model.dto.PessoaDto;
import org.assertj.core.util.Sets;

import java.time.LocalDate;
import java.util.Arrays;

public class PessoaEnvironment {

    public static PessoaEntity obterEntity(Long id) {
        return new PessoaEntity()
                .setId(id)
                .setNome("Pessoa Test")
                .setCpf("33333333333")
                .setDataNascimento(LocalDate.of(2000, 1, 1))
                .setContatos(Sets.newHashSet(
                        Arrays.asList(new ContatoEntity()
                                .setNome("Contato Teste")
                                .setTelefone("19999999999")
                                .setEmail("contato@test.com"))
                ));
    }

    public static PessoaDto obterDto(Long id) {
        return new PessoaDto()
                .setId(id)
                .setNome("Pessoa Test")
                .setCpf("33333333333")
                .setDataNascimento(LocalDate.of(2000, 1, 1))
                .setContatos(Sets.newHashSet(
                        Arrays.asList(new ContatoDto()
                                .setNome("Contato Teste")
                                .setTelefone("19999999999")
                                .setEmail("contato@test.com"))
                ));
    }

}
