package br.com.elotech.crudservice.environment;

import br.com.elotech.crudservice.model.dto.FiltroConsultaPessoaDto;

import java.time.LocalDate;

public class FiltroConsultaEnvironment {

    public static FiltroConsultaPessoaDto obter(String nome, String cpf, LocalDate dataNascimento, Long idPessoa) {
        return new FiltroConsultaPessoaDto()
                .setIdPessoa(idPessoa)
                .setNome(nome)
                .setDataNascimento(dataNascimento)
                .setCpf(cpf);
    }
}
