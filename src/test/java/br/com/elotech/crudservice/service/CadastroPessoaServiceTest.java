package br.com.elotech.crudservice.service;

import br.com.elotech.crudservice.environment.FiltroConsultaEnvironment;
import br.com.elotech.crudservice.environment.PessoaEnvironment;
import br.com.elotech.crudservice.model.domain.PessoaEntity;
import br.com.elotech.crudservice.model.dto.FiltroConsultaPessoaDto;
import br.com.elotech.crudservice.model.dto.PessoaDto;
import br.com.elotech.crudservice.repository.PessoaFiltroRepository;
import br.com.elotech.crudservice.repository.PessoaRepository;
import br.com.elotech.crudservice.utils.Exceptions.NotFoundException;
import br.com.elotech.crudservice.utils.enums.MensagemSistemaEnum;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class CadastroPessoaServiceTest {

    @InjectMocks
    private CadastroPessoaService service;
    @Mock
    private PessoaRepository pessoaRepository;
    @Mock
    private PessoaFiltroRepository pessoaFiltroRepository;
    @Mock
    private ConversionService conversionService;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void obterPessoaByIdTest() {
        Long id = 1L;
        PessoaEntity pessoaEntity = PessoaEnvironment.obterEntity(1L);
        PessoaDto pessoaDto = PessoaEnvironment.obterDto(1L);

        when(pessoaRepository.findById(id)).thenReturn(Optional.of(pessoaEntity));
        when(conversionService.convert(pessoaEntity, PessoaDto.class)).thenReturn(pessoaDto);

        PessoaDto result = assertDoesNotThrow(() -> service.obterPessoaById(id));
        verify(pessoaRepository, times(1)).findById(id);

        assertNotNull(result);
    }

    @Test
    public void obterPessoaByIdNaoEncontradaTest() {
        Long id = 1L;
        when(pessoaRepository.findById(id)).thenReturn(Optional.empty());

        NotFoundException excecao = assertThrows(NotFoundException.class, () -> service.obterPessoaById(id));
        verify(pessoaRepository, times(1)).findById(id);

        assertNotNull(excecao);
        assertThat(excecao.getMessage(), is(String.format(MensagemSistemaEnum.CADASTRO_PESSOA_NAO_ENCONTRADO.getValue(), id)));
    }

    @Test
    public void obterPessoasByFiltroTest() {
        FiltroConsultaPessoaDto filtro = FiltroConsultaEnvironment.obter("Nome", null, null, null);
        PageRequest page = PageRequest.of(0, 10, Sort.Direction.ASC, "nome");
        PessoaEntity pessoaEntity = PessoaEnvironment.obterEntity(1L);
        PessoaDto pessoaDto = PessoaEnvironment.obterDto(1L);

        when(pessoaFiltroRepository.findByFiltro(filtro, page)).thenReturn(new PageImpl<>(Arrays.asList(pessoaEntity)));
        when(conversionService.convert(pessoaEntity, PessoaDto.class)).thenReturn(pessoaDto);

        assertDoesNotThrow(() -> service.obterPessoasByFiltro(filtro, 0, 10));
        verify(pessoaFiltroRepository, times(1)).findByFiltro(filtro, page);
    }

    @Test
    public void cadastrarPessoaTest() {
        PessoaEntity pessoaEntity = PessoaEnvironment.obterEntity(1L);
        PessoaDto pessoaDto = PessoaEnvironment.obterDto(1L);

        when(conversionService.convert(pessoaDto, PessoaEntity.class)).thenReturn(pessoaEntity);
        when(pessoaRepository.saveAndFlush(pessoaEntity)).thenReturn(pessoaEntity);
        when(conversionService.convert(pessoaEntity, PessoaDto.class)).thenReturn(pessoaDto);

        PessoaDto result = assertDoesNotThrow(() -> service.cadastrarPessoa(pessoaDto));
        verify(pessoaRepository, times(1)).saveAndFlush(pessoaEntity);

        assertNotNull(result);
    }

    @Test
    public void alterarPessoaTest() {
        PessoaEntity pessoaEntityAtual = PessoaEnvironment.obterEntity(1L);
        pessoaEntityAtual.setCpf("11111111111");
        pessoaEntityAtual.setNome("Teste Nome");
        PessoaEntity pessoaEntityAnterior = PessoaEnvironment.obterEntity(1L);
        PessoaDto pessoaDto = PessoaEnvironment.obterDto(1L);
        pessoaDto.setCpf("11111111111");
        pessoaDto.setNome("Teste Nome");

        when(conversionService.convert(pessoaDto, PessoaEntity.class)).thenReturn(pessoaEntityAtual);
        when(pessoaRepository.findById(pessoaEntityAtual.getId())).thenReturn(Optional.of(pessoaEntityAnterior));
        when(pessoaRepository.saveAndFlush(pessoaEntityAtual)).thenReturn(pessoaEntityAtual);
        when(conversionService.convert(pessoaEntityAtual, PessoaDto.class)).thenReturn(pessoaDto);

        PessoaDto result = assertDoesNotThrow(() -> service.alterarPessoa(1L, pessoaDto));
        verify(pessoaRepository, times(1)).saveAndFlush(pessoaEntityAtual);

        assertNotNull(result);
        assertThat(result.getCpf(), is(pessoaEntityAtual.getCpf()));
        assertThat(result.getNome(), is(pessoaEntityAtual.getNome()));
    }

    @Test
    public void alterarPessoaSemAlteracaoCadastroTest() {
        PessoaEntity pessoaEntityAtual = PessoaEnvironment.obterEntity(1L);
        PessoaEntity pessoaEntityAnterior = PessoaEnvironment.obterEntity(1L);
        PessoaDto pessoaDto = PessoaEnvironment.obterDto(1L);

        when(conversionService.convert(pessoaDto, PessoaEntity.class)).thenReturn(pessoaEntityAtual);
        when(pessoaRepository.findById(pessoaEntityAtual.getId())).thenReturn(Optional.of(pessoaEntityAnterior));
        when(pessoaRepository.saveAndFlush(pessoaEntityAtual)).thenReturn(pessoaEntityAtual);
        when(conversionService.convert(pessoaEntityAtual, PessoaDto.class)).thenReturn(pessoaDto);

        PessoaDto result = assertDoesNotThrow(() -> service.alterarPessoa(1L, pessoaDto));
        verify(pessoaRepository, never()).saveAndFlush(pessoaEntityAtual);

        assertNotNull(result);
    }

    @Test
    public void alterarPessoaCadastroNaoEncontradoTest() {
        PessoaEntity pessoaEntityAtual = PessoaEnvironment.obterEntity(1L);
        PessoaDto pessoaDto = PessoaEnvironment.obterDto(1L);

        when(conversionService.convert(pessoaDto, PessoaEntity.class)).thenReturn(pessoaEntityAtual);
        when(pessoaRepository.findById(pessoaEntityAtual.getId())).thenReturn(Optional.empty());

        NotFoundException excecao = assertThrows(NotFoundException.class, () -> service.alterarPessoa(1L, pessoaDto));
        verify(pessoaRepository, times(1)).findById(pessoaDto.getId());
        verify(pessoaRepository, never()).saveAndFlush(pessoaEntityAtual);
        verify(conversionService, never()).convert(pessoaEntityAtual, PessoaDto.class);

        assertNotNull(excecao);
        assertThat(excecao.getMessage(), is(String.format(MensagemSistemaEnum.CADASTRO_PESSOA_NAO_ENCONTRADO.getValue(), pessoaDto.getId())));
    }

    @Test
    public void removerPessoaTest() {
        Long id = 1L;
        PessoaEntity cadastroPessoa = PessoaEnvironment.obterEntity(1L);

        when(pessoaRepository.findById(id)).thenReturn(Optional.of(cadastroPessoa));

        assertDoesNotThrow(() -> service.removerPessoa(id));
        verify(pessoaRepository, times(1)).delete(cadastroPessoa);
    }

    @Test
    public void removerPessoaCadastroNaoEncontroTest() {
        Long id = 1L;

        when(pessoaRepository.findById(id)).thenReturn(Optional.empty());

        NotFoundException excecao = assertThrows(NotFoundException.class, () -> service.removerPessoa(id));
        verify(pessoaRepository, never()).delete(any());

        assertNotNull(excecao);
        assertThat(excecao.getMessage(), is(String.format(MensagemSistemaEnum.CADASTRO_PESSOA_NAO_ENCONTRADO.getValue(), id)));
    }
}